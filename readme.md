# Yii2 WebSocket
Компонент для Yii2 для использования функционала websocket через консольные контроллеры. 
Также предоставляется стандартизированный формат общения между front и backend частей приложения с помощью моделей.
Компонент основан на consik/yii2websocket, который основан на cboden/ratchet

### Подключение
В файле консольной конфигурации приложения (для Yii2 basic это /backend/config/console.php) добавить следующие строки:
```text
<?php
$config = [
    ...
    'bootstrap' => [
        'socket',
    ],
    'components' => [
        'socket' => [
            'class' => socket\Socket::class,
            
            //не обязательные параметры, выставленные по умолчанию
            'socketServerClass' => socket\SocketServer::class,
            'port' => socket\SocketController::DEFAULT_SOCKET_PORT,
        ],
    ],
    ...
];
```

### Запуск
```text
php yii socket/start
```
Теперь сокет-сервер слушает обращения и адресует их консольным контроллерам

### Использование
Создайте в консольном контроллере метод с такими входными параметрами:
```text
public function actionTest(consik\yii2websocket\events\WSClientMessageEvent $event, socket\models\SocketRequestForm $request): void
{
    ...
    //Выполнение операций
    ...
    
    //Создание ответа с данными
    $data = []; //любой вид данных, в том числе Model или ActiveRecord
    $dataResponse = (new socket\models\SocketResponseForm($request))->setData($data)->toString();
    
    //Создание ответа с ошибкой
    $error = 'Ошибка';
    $errorResponse = (new socket\models\SocketResponseForm($request))->setError($error)->toString();
    
    //Создание ответа с текстом
    $message = 'Текст';
    $messageResponse = (new socket\models\SocketResponseForm($request))->setMessage($message)->toString();
    
    //Отправка ответа клиенту
    $event->client->send($dataResponse);
    //Можно отправлять сразу несколько сообщений
    $event->client->send($errorResponse);
    $event->client->send($messageResponse);
}
```

Обращение к контроллеру через javascript:
```text
<!DOCTYPE html>
<html>
    <body>
        <script>
            const serverAddress = '127.0.0.1'; 
            let conn = new WebSocket('ws://' + serverAddress);
            conn.onmessage = function(e) {
                console.log('Response:' + e.data);
            };
            conn.onopen = function(e) {
                console.log('Соединение установлено!');
                conn.send('{"action":"test/test", "data": {"key": "value"}}');
            };
        </script>
    </body>
</html>
```

Для расширения функционала и использования event-ов 
по типу EVENT_WEBSOCKET_OPEN или EVENT_CLIENT_ERROR,
можно расширить функционал класса socket\SocketServer,
обратившись к документации компонента consik/yii2-websocket
(https://github.com/consik/yii2-websocket/blob/master/README.md)
и указать свой кастомный класс в консольной конфигурации
приложения в поле components->socket->class
```text
<?php
$config = [
    ...
    'components' => [
        'socket' => [
            'class' => project\MyCustomSocketServer::class,
            ...
        ],
    ],
    ...
];
```