<?php

namespace socket\models;

use yii\base\Model;

class SocketRequestForm extends Model
{

    /**
     * @var string
     */
    public $action;

    /**
     * @var array
     */
    public $data;

    public function rules(): array
    {
        return [
            [['action'], 'string'],
            [['action'], 'required'],
            [['data'], 'safe'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'action' => 'Контроллер/Метод',
            'data' => 'Данные',
        ];
    }
}