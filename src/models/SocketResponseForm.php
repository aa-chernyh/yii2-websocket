<?php

namespace socket\models;

use yii\base\Model;

class SocketResponseForm extends SocketRequestForm
{

    /**
     * @var string[]
     */
    public $errors;

    /**
     * @var string[]
     */
    public $messages;

    /**
     * @var boolean
     */
    public $result = true;

    public function __construct(SocketRequestForm $request)
    {
        parent::__construct([]);
        $this->action = $request->action;
    }

    public function setError(string $error): self
    {
        $this->errors[] = $error;
        $this->result = false;
        return $this;
    }

    /**
     * @param string[] $errors
     * @return $this
     */
    public function setErrors(array $errors): self
    {
        foreach ($errors as $error) {
            $this->setError($error);
        }
        return $this;
    }

    public function setData($data): self
    {
        if($data instanceof Model) {
            $this->data = $data->attributes;
        } else {
            $this->data = $data;
        }

        return $this;
    }

    public function toString(): string
    {
        return json_encode($this->attributes);
    }

    public function setMessage(string $message): self
    {
        $this->messages[] = $message;
        return $this;
    }

    /**
     * @param string[] $messages
     * @return $this
     */
    public function setMessages(array $messages): self
    {
        foreach ($messages as $message) {
            $this->setMessage($message);
        }
        return $this;
    }

    public function setException(\Throwable $exception): self
    {
        $message = 'File: ' . $exception->getFile()
            . ', line: ' . $exception->getLine()
            . ', message: ' . $exception->getMessage();
        return $this->setError($message);
    }
}