<?php

namespace socket;

use socket\exceptions\LogicException;
use Yii;
use yii\base\BootstrapInterface as BootstrapInterfaceAlias;
use yii\console\Application as ConsoleApp;
use yii\base\Component;
use yii\console\Application;
use yii\helpers\Inflector;

class Socket extends Component implements BootstrapInterfaceAlias
{

    /**
     * @var string
     */
    public $socketServerClass;

    /**
     * @var integer
     */
    public $port;

    /**
     * @param Application $app
     * @throws LogicException
     */
    public function bootstrap($app)
    {
        if ($app instanceof ConsoleApp) {
            $app->controllerMap[$this->getCommandId()] = [
                'class' => SocketController::class,
                'socketServerClass' => $this->socketServerClass,
                'port' => $this->port,
            ];
        }

    }

    /**
     * @return string
     * @throws LogicException
     */
    protected function getCommandId(): string
    {
        foreach (Yii::$app->getComponents(false) as $id => $component) {
            if ($component === $this) {
                return Inflector::camel2id($id);
            }
        }
        throw new LogicException('Queue must be an application component');
    }

}