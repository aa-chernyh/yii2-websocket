<?php

namespace socket;

use socket\exceptions\LogicException;
use socket\exceptions\ValidatorException;
use socket\models\SocketRequestForm;
use socket\models\SocketResponseForm;
use consik\yii2websocket\events\WSClientMessageEvent;
use consik\yii2websocket\WebSocketServer;
use Throwable;
use Yii;
use yii\helpers\Inflector;

class SocketServer extends WebSocketServer
{

    public function init()
    {
        parent::init();
        $this->onMessageEvent();
    }

    protected function onMessageEvent(): void
    {
        $this->on(self::EVENT_CLIENT_MESSAGE, function (WSClientMessageEvent $event) {
            try {
                $socketForm = $this->parseMessage($event->message);
                list($controller, $actionID) = Yii::$app->createController($socketForm->action);
                $method = 'action' . ucfirst(Inflector::id2camel($actionID));
                if(!method_exists($controller, $method)) {
                    $errorMessage = 'Нет метода ' . $method . ' в контроллере ' . get_class($controller);
                    throw new LogicException($errorMessage);
                }
                $controller->{$method}($event, $socketForm);
            } catch (Throwable $exception) {
                $response = (new SocketResponseForm())->setException($exception)->toString();
                $event->client->send($response);
            }
        });
    }

    /**
     * @param string $socketMessage
     * @return SocketRequestForm
     * @throws ValidatorException
     */
    private function parseMessage(string $socketMessage): SocketRequestForm
    {
        $socketForm = new SocketRequestForm();
        $socketForm->load(json_decode($socketMessage, true), '');
        if (!$socketForm->validate()) {
            $errorMessage = 'Неверный формат данных: ' . array_values($socketForm->getFirstErrors())[0];
            throw new ValidatorException($errorMessage);
        }

        return $socketForm;
    }
}