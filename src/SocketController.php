<?php

namespace socket;

use Yii;
use yii\base\InvalidConfigException;
use yii\console\Controller;

class SocketController extends Controller
{

    const DEFAULT_SOCKET_PORT = 9031;

    /**
     * @var string
     */
    public $socketServerClass;

    /**
     * @var integer
     */
    public $port;

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);

        if(!$this->socketServerClass) {
            $this->socketServerClass = SocketServer::class;
        }

        if(!$this->port) {
            $this->port = self::DEFAULT_SOCKET_PORT;
        }
    }

    /**
     * @throws InvalidConfigException
     */
    public function actionStart(): void
    {
        $socketServer = Yii::createObject($this->socketServerClass);
        $socketServer->port = $this->port;
        $socketServer->start();
    }
}